package me.sergey.fakeortrue_app

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate

@Composable
fun FinishScreen(navController: NavHostController) {
    Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
                text = "Раунд звершен",
                style = MaterialTheme.typography.h5,
                modifier = Modifier.padding(bottom = 50.dp)
        )
        Text(
                text = "Ваш счет: $score",
                style = MaterialTheme.typography.subtitle1,
                modifier = Modifier.padding(bottom = 25.dp)
        )
        Button(onClick = {navController.navigate("main")}) {
            Text("В главное меню")
        }
    }
}