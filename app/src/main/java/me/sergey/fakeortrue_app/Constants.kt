package me.sergey.fakeortrue_app

const val scoreForOnlyTitleAnswer = 20
const val scoreForAnswerWithHelp = 10
const val scorePenaltyForWrongAnswer = -10
const val waitingTimeForLoadingArticle = 8000L
