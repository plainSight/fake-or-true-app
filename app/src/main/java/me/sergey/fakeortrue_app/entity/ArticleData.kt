package me.sergey.fakeortrue_app.entity

data class ArticleData(
    val link: String,
    val title: String,
    val content: String,
    val imageLink: String,
    val source: String,
    val publicationDate: String,
    val truth: Boolean
)
