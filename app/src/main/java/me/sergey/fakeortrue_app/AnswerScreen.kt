package me.sergey.fakeortrue_app

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate
import me.sergey.fakeortrue_app.ui.theme.customGreen
import me.sergey.fakeortrue_app.ui.theme.customRed

@Composable
fun AnswerScreen(navController: NavHostController, isCorrect: Boolean) {
    val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Open))
    val text = if (isCorrect) "Правильный ответ!" else "Неправильно!"
    val color = if (isCorrect) customGreen else customRed
    Scaffold(
            scaffoldState = scaffoldState,
            topBar = { topAppBar(navController, true) },
            content = {
                Column(
                        modifier = Modifier.fillMaxSize(),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                            text = text,
                            style = MaterialTheme.typography.h5,
                            modifier = Modifier.padding(bottom = 50.dp),
                            color = color
                    )
                    Button(onClick = { navController.navigate("beginner") }) {
                        Text("Далее")
                    }
                }
            }
    )
}