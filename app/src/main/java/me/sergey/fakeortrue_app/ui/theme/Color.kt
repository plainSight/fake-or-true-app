package me.sergey.fakeortrue_app.ui.theme

import androidx.compose.ui.graphics.Color

val purple200 = Color(0xFFBB86FC)
val purple500 = Color(0xFF6200EE)
val purple700 = Color(0xFF3700B3)
val teal200 = Color(0xFF03DAC5)
val customGreen = Color(0xFF009933)
val customRed = Color(0xFFB00020)