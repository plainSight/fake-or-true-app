package me.sergey.fakeortrue_app

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun MainScreenButton(name: String, action: () -> Unit) {
    Spacer(modifier = Modifier.padding(5.dp))
    Button(
            onClick = action,
            modifier = Modifier
                    .width(140.dp)
                    .height(50.dp)
    ) {
        Text(name)
    }
    Spacer(modifier = Modifier.padding(15.dp))
}