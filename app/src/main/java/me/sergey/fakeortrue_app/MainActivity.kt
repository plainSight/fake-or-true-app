package me.sergey.fakeortrue_app

import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.navigation.compose.*
import android.os.Bundle
import me.sergey.fakeortrue_app.ui.theme.FakeOrTrue_AppTheme


var score = 0

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            FakeOrTrue_AppTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    EmptyScreen()
                }
            }
        }
    }

    @Composable
    fun EmptyScreen() {
        val navController = rememberNavController()
        NavHost(navController = navController, startDestination = "main") {
            composable("main") { MainScreen(navController) }
            composable("beginner") { BeginnerScreen(navController) }
            composable("right") { AnswerScreen(navController, true) }
            composable("wrong") { AnswerScreen(navController, false) }
            composable("finish") { FinishScreen(navController) }
            composable("learning") { LearningScreen(navController) }
            composable("profile") { ProfileScreen(navController) }
        }
    }
















}