package me.sergey.fakeortrue_app

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.*
import dev.chrisbanes.accompanist.coil.CoilImage
import kotlinx.coroutines.runBlocking
import me.sergey.fakeortrue_app.entity.ArticleData
import me.sergey.fakeortrue_app.ui.theme.customGreen
import me.sergey.fakeortrue_app.ui.theme.customRed

private var article: ArticleData? = null
private var isHelpUsed = false

@Composable
fun BeginnerScreen(navController: NavHostController) {
    val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Open))
    article = runBlocking { getNewArticle() }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = { topAppBar(navController, true) },
        content = { content(navController, article) },
        bottomBar = { bottomAppBar(navController) },
    )
}

@Composable
fun topAppBar(navController: NavHostController, scoreProvided: Boolean) {
    TopAppBar(
        title = {
            Row {
                IconButton(onClick = { navController.navigate("main") }) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "Return",
                        tint = Color.White
                    )
                }
                Text(
                    text = "Fake or true?",
                    modifier = Modifier
                        .weight(1f)
                        .padding(start = 4.dp)
                        .wrapContentWidth(Alignment.Start),
                    color = Color.White
                )
                if (scoreProvided)
                    Text(
                        text = "Score: $score",
                        modifier = Modifier
                            .weight(1f)
                            .padding(end = 4.dp)
                            .wrapContentWidth(Alignment.End),
                        color = Color.White
                    )
            }
        },
        backgroundColor = Color.Blue
    )
}

@Composable
fun content(navController: NavHostController, article: ArticleData?) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        if (article == null) {
            Column {
                Text("Проверьте свое подключение к интернету.")
                Button(onClick = { navController.navigate("beginner") }) {
                    Text("Попробовать снова")
                }
            }
        } else {
            Text(
                modifier = Modifier.padding(16.dp),
                text = article.title,
                style = MaterialTheme.typography.h5
            )

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .verticalScroll(rememberScrollState())
                    .animateContentSize(),
            ) {

                CoilImage(
                    data = article.imageLink,
                    contentDescription = "Article photo",
                    fadeIn = true,
                    loading = {
                        Box(Modifier.matchParentSize()) {
                            CircularProgressIndicator(Modifier.align(Alignment.Center))
                        }
                    },
                    modifier = Modifier
                        .padding(20.dp)
                        .fillMaxSize()
                )

                AdditionalContent()
            }
        }
    }
}

@Composable
fun AdditionalContent() {
    if (article == null) return

    var isAdditionalContentOpened by remember { mutableStateOf(false) }

    if (!isAdditionalContentOpened)
        Button(
            onClick = {
                isAdditionalContentOpened = !isAdditionalContentOpened
            }
        ) {
            Text("Показать новость целиком")
        }
    else
        Column(modifier = Modifier.fillMaxSize()) {
            Text(
                modifier = Modifier.padding(20.dp),
                text = article!!.content,
                style = MaterialTheme.typography.body1
            )
        }

    isHelpUsed = isAdditionalContentOpened
}

@Composable
fun bottomAppBar(navController: NavHostController) {
    if (article == null) return
    BottomAppBar(backgroundColor = Color.White, modifier = Modifier.height(100.dp)) {
        Column {
            Text(
                text = "Дата публикации новости: ${article?.publicationDate}",
                modifier = Modifier.padding(bottom = 16.dp)
            )
            Row {
                TrueFalseButton(
                    bool = true,
                    navController = navController,
                    Modifier
                        .weight(1f)
                        .padding(start = 4.dp)
                        .wrapContentWidth(Alignment.Start)
                )
                TrueFalseButton(
                    bool = false,
                    navController = navController,
                    Modifier
                        .weight(1f)
                        .padding(end = 4.dp)
                        .wrapContentWidth(Alignment.End)
                )

            }
        }
    }
}

@Composable
fun TrueFalseButton(bool: Boolean, navController: NavHostController, modifier: Modifier) {
    if (article == null) return
    val buttonColor = if (bool) customGreen else customRed
    Button(
        onClick = {
            if (article?.truth == bool) {
                score += if (isHelpUsed) scoreForAnswerWithHelp else scoreForOnlyTitleAnswer
                navController.navigate("right")
            } else {
                score += scorePenaltyForWrongAnswer
                navController.navigate("wrong")
            }
        },
        modifier = modifier
            .padding(10.dp, 0.dp)
            .fillMaxWidth(),
        colors = ButtonDefaults
            .buttonColors(backgroundColor = buttonColor),
    ) {
        Text(
            textAlign = TextAlign.Center,
            text = if (bool) "True" else "False",
            color = Color.White,
            modifier = Modifier.weight(1f)
        )
    }
}
