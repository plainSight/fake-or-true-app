package me.sergey.fakeortrue_app

import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.navigate

@Composable
fun MainScreen(navController: NavHostController) {
    score = 0
    Column(
            modifier = Modifier.padding(30.dp).fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
                "Fake or True",
                style = MaterialTheme.typography.h2
        )
        Column(
                modifier = Modifier.padding(bottom = 50.dp).fillMaxHeight(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
        ) {
            MainScreenButton(name = "Beginner", action = {
                navController.navigate("beginner")
            })
            MainScreenButton(name = "Medium", action = {  })
            MainScreenButton(name = "Professor", action = {  })
            MainScreenButton(name = "Правила игры", action = {
                navController.navigate("learning")
            })
            MainScreenButton(name = "Полезное", action = {  })
        }
    }
}