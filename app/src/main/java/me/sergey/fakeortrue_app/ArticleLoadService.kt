package me.sergey.fakeortrue_app

import com.beust.klaxon.Klaxon
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import me.sergey.fakeortrue_app.entity.ArticleData
import java.net.URL
import kotlin.concurrent.thread

var newArticle: ArticleData? = null

suspend fun getNewArticle(): ArticleData? {
    if (newArticle == null)
        newArticle = loadArticle()
    return newArticle.also { newArticle = loadArticle() }
}

private suspend fun loadArticle(): ArticleData? = withContext(Dispatchers.IO) {
    var data: ArticleData? = null
    thread {
        while (data == null) {
            try {
                val text = URL("http://176.119.159.91:7000/random").readText()
                data = Klaxon().parse<ArticleData>(text)
            } catch (e: Exception) {
                println("something went wrong")
            }
        }
    }.join(waitingTimeForLoadingArticle)

    println("Article loaded!")
    return@withContext data
}